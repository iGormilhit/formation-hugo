Formation interne à [rero](https://rero.ch) pour l'édition du blog
[rero21](https://rero21.ch) ou [sonar](https://sonar.ch).

Liste des choses à aborder:

- [x] [text/markdown editor](editors.md)
    - paramétrage de l'éditeur
        - [x] support de la syntaxe MarkDown
        - [x] HUGO helpers
        - [ ] Soft, Hard wrap
- syntaxe MarkDown
- HUGO
    - hugo new
    - tags, draft
    - content
    - shortcodes (smallcaps, table, figure)
- `git`
    - fork, clone
    - branch
    - PR
        - [ ] création
        - [ ] révision
        - [ ] intégration
    - submodule

Pré-requis:

- sont installés: git et hugo, un [éditeur de texte](editors.md)
- savoir trouver le terminal et le dossier de travail
- avoir un compte github configuré, avec une clé ssh
