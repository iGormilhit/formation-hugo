## Éditeurs de texte

HUGO supporte [plusieurs 
syntaxes](https://gohugo.io/content-management/formats/) de mise en forme du
texte, la plus souvent utilisée étant le MarkDown. Ce qui revient à éditeur un
fichier au format texte. Pour ce faire, on utilise un éditeur de texte, par
opposition à un traitement de texte. Ci-dessous une liste d'éditeurs à
envisager et quelques conseils de configuration. Le but étant d'obtenir un
éditeur qui supporte la syntaxe MarkDown et soit éventuellement capable d'aider
à la rédaction, notamment des tables. On peut considérer que la fonction de
prévisualisation est proposée par HUGO lui-même.

### Notepad++

C'est un éditeur souvent utilisé sous Windows. On peut citer deux extensions
pour MarkDown :

1. [Markdown Syntax Highlighting for
   Notepad++](https://github.com/Edditoria/markdown-plus-plus) qui permet le
   support de la syntaxe.
2. [MarkdownViewerPlusPlus](https://github.com/nea/MarkdownViewerPlusPlus)
   pour la prévisualisation.

Je n'ai pas trouvé d'extension pour aider à la création de table en MarkDown,
ni de *helpers* pour HUGO.

### VSCodium

VSCodium est une compilation automatique de VSCode, afin d'obtenir un binaire
qui soit sous license libre et qui n'embarque pas les outils de télémtrie de
Microsoft, particulièrement agressifs. En dehors de ça, c'est un excellent
éditeur multiplateforme.

Pour l'installation : https://github.com/VSCodium/vscodium#downloadinstall

#### MarkDown

VSCodium supporte nativement la syntaxe MarkDown. On trouve sur la page
suivante des informations utiles :
https://code.visualstudio.com/docs/languages/markdown. Sur le *market place* on
peut également trouver des extensions pour améliorer la lisibilité des
tableaux :
https://marketplace.visualstudio.com/search?term=markdown%20table%20formatter&target=VSCode&category=All%20categories&sortBy=Relevance.
Par contre, je n'ai pas encore trouvé d'extension capable de formatter à la
volée (pendant la rédaction) une table, ce qui existe sur Atom ou vim/neovim.

#### HUGO

Il y a plusieurs extensions spécifiques à VSCode, mais je pense que
[hugofy](https://marketplace.visualstudio.com/items?itemName=akmittal.hugofy)
est celle qui a le plus d'utilité. Elle permet, notamment, de créer des
nouveaux contenus (avec des métadonnées préremplies), de lancer le serveur
local pour visualiser le résultat.

### Atom

Atom est également un très bon éditeur multiplateforme supportant nativement la
syntaxe MarkDown. Pour l'installer :
https://flight-manual.atom.io/getting-started/sections/installing-atom/

#### MarkDown

La documentation officielle pour l'utilisation de MarkDown est très bien
faite : https://flight-manual.atom.io/using-atom/sections/writing-in-atom/ À
noter la prévisualisation intégrée et des snippets qui peuvent s'avérer très
utiles.

À cela s'ajoute l'extension
[markdown-table-editor](https://atom.io/packages/markdown-table-editor) qui
améliore l'affichage dans l'éditeur de la table à la volée, ce qui simplifie
passablement la tâche. Si vous trouvez une extension équivalente pour VSCodium
ou Notepad ++, n'hésitez pas à l'utiliser et à m'en informer[^1] ! Regardez ce
que ça donne :

![](https://i.github-camo.com/c2a810dc038c3fb1a3d3373fc6ae83c51112997f/68747470733a2f2f6769746875622e636f6d2f7375736973752f6d61726b646f776e2d7461626c652d656469746f722f77696b692f696d616765732f64656d6f2e676966)

#### HUGO

L'extension [hugofy](https://atom.io/packages/hugofy) existe aussi pour Atom,
avec les mêmes fonctionnalités utiles que pour VSCodium[^2].

[^1]: Pour `vim` et `neovim`, il s'agit de
[vim-table-mode](https://github.com/dhruvasagar/vim-table-mode)

[^2]: Bien entenu, on trouve aussi `hugofy` pour SublimeText.
